import {Injectable } from '@angular/core';
import {HttpService} from "../http.service";
import {environment} from "../../environments/environment";

@Injectable()
export class TreeService {



  constructor(private httpService : HttpService) {

  }


  getTree(){
    return this.httpService.get(environment.tree)
  }

}

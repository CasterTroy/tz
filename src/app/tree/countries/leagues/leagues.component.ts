import { Component, OnInit, Input} from '@angular/core';
import {LeaguesService} from "./leagues.service";
import {GridService} from "../../../grid/grid.service";


@Component({
  selector: 'tz-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.css']
})
export class LeaguesComponent implements OnInit {
  @Input() _leagues: any;
  constructor(private leaguesService : LeaguesService,
              private gridService : GridService) { }

  ngOnInit() {

  }

  getMatches(index: number){
    this.leaguesService.getLeague(this._leagues[index].id).subscribe(
      data=>{
        this.gridService.changeMatches(data.data)
      }
    )
  }
}

import { Injectable } from '@angular/core';
import {HttpService} from "../../../http.service";
import {environment} from "../../../../environments/environment";

@Injectable()
export class LeaguesService {

  constructor(private httpService: HttpService) { }

  getLeague(id: string){
    return this.httpService.get(environment.leagues+id+'.json')
  }
}

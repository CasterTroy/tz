import { Component, OnInit, Input } from '@angular/core';


declare var $: any;

@Component({
  selector: 'tz-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  @Input() _countries: any;
  @Input() _parent: string;
  constructor() { }

  ngOnInit() {


  }

  show(index: number){
    var element = $(this._parent+' .countries__item:eq('+index+')').find('.countries-module');

    if(element.hasClass('active')){
      element.removeClass('active')
      return
    }
    $(".countries-module").removeClass('active')
    element.addClass('active');
  }

}

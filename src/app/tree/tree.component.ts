import { Component, OnInit } from '@angular/core';
import {TreeService} from "./tree.service";


declare var $: any;
@Component({
  selector: 'tz-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {
  private _tree: any;

  constructor(private treeService : TreeService) { }

  ngOnInit() {

  /** get json tree**/
    this.treeService.getTree().subscribe(
      data =>{
        this._tree = data;

        /*sort srots*/
        this._tree.data.sort(this.comparee);

        /*sort countries*/
          this._tree.data.forEach((item, i) => {
            item.countries.sort(this.compareeName)

        /*sort leagues*/
            if(item.countries[i]){
              item.countries[i].leagues.sort(this.compareeName)
            }
          });
      }
    )
  }


/*show or hide menu*/
  show(index: number){
    var element = $('.tree__item:eq('+index+')').find('.tree-module');
    if(element.hasClass('active')){
      element.removeClass('active')
      return
    }
    $(".tree-module").removeClass('active')
    element.addClass('active');
  }
/*sort for priority*/
  comparee(a: any, b: any) {
    return b.priority - a.priority;
  }
/*alphabetizing*/
  compareeName(a: any, b: any) {
    return b.name < a.name;
  }


}

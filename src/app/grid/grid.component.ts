import { Component, OnInit } from '@angular/core';
import { GridService } from "./grid.service";


declare var moment: any;

@Component({
  selector: 'tz-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  private _layout: any;
  private _thead: any = [];

  public _matches: any;
  private _tbody: any = [];
  private _odds: any = [];

  constructor(private gridService : GridService ) { }

  ngOnInit() {
    this.gridService.matches.subscribe(
      data => this._matches = data
    )

  /** get layout **/
    this.gridService.getLayout().subscribe(
      data =>{
        this._layout = data.data;
        this._layout.sort(this.compareeMin)
        this._layout.forEach((item) => {
          item.odds.sort(this.compareeMin)
          item.odds.forEach((element)=>{
            this._thead.push(element)
          })
        })
      })

  /** get matches **/
  this.gridService.getMatches().subscribe(
    data =>{
      this._matches = data.data
      this._matches.forEach((item)=>{
        item.sd = moment(item.sd).format('MM-DD HH:mm')
      })
    }
  )

  }
  /*chenge matches*/
  chengeMatches(newMatches: any){

    this._matches = newMatches;
    this._matches.forEach((item)=>{
      item.sd = moment(item.sd).format('MM-DD HH:mm')
      console.log(item.sd)
    })
  }


  /*sort for priority*/
  compareeMax(a: any, b: any) {
    return b.priority - a.priority;
  }

  compareeMin(a: any, b: any) {
    return  a.priority - b.priority
  }

}

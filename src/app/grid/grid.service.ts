import { Injectable } from '@angular/core';
import {HttpService} from "../http.service";
import {environment} from "../../environments/environment";
import {EventEmitter} from "@angular/common/src/facade/async";

@Injectable()
export class GridService {
  constructor(private httpService: HttpService) { }

  getLayout(){
    return this.httpService.get(environment.layout)
  }

  getMatches(){
    return this.httpService.get(environment.matches)
  }


  matches = new EventEmitter<any>();
  changeMatches(value: any){
    this.matches.emit(value);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/*services*/
import { HttpService } from "./http.service";
import { TreeService } from "./tree/tree.service";
import { GridService } from "./grid/grid.service";



/*components*/
import { AppComponent } from './app.component';
import { TreeComponent } from './tree/tree.component';
import { GridComponent } from './grid/grid.component';
import { CountriesComponent } from './tree/countries/countries.component';
import { LeaguesComponent } from './tree/countries/leagues/leagues.component';
import { LeaguesService } from "./tree/countries/leagues/leagues.service";







@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    GridComponent,
    CountriesComponent,
    LeaguesComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [HttpService,
              TreeService,
              GridService,
              LeaguesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

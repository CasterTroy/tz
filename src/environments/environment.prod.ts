export const environment = {
  production: true,
  host: './app/mocks/',
  layout: 'layout.json',
  matches: 'matches.json',
  tree: 'tree.json'
};

import { FdaPage } from './app.po';

describe('fda App', function() {
  let page: FdaPage;

  beforeEach(() => {
    page = new FdaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('tz works!');
  });
});

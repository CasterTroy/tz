import { browser, element, by } from 'protractor';

export class FdaPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('tz-root h1')).getText();
  }
}
